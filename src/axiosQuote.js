import axios from 'axios';

const axiosQuote = axios.create({
    baseURL: 'https://jsgroup7exam.firebaseio.com/'
});

export default axiosQuote;