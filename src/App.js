import React from 'react';
import {Route, Switch, BrowserRouter, NavLink} from 'react-router-dom';
import Quotes from "./containers/Quotes/Quotes";
import SubmitNewQuote from "./containers/SubmitNewQuote/SubmitNewQuote";
import EditQuote from "./containers/EditQuote/EditQuote";
import SelectedQuote from "./containers/SelectedQuote/SelectedQuote";

const App = () => {
    return (
        <BrowserRouter>
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/">Quotes</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to={"/submit"}>Submit new quote</NavLink>
                        </li>
                    </ul>
                </div>
            </nav>
            <Switch>
                <Route path="/" exact component={Quotes}/>
                <Route path="/submit" exact component={SubmitNewQuote}/>
                <Route path="/quote/:id/edit" component={EditQuote}/>
                <Route path="/star-wars" component={SelectedQuote}/>
                <Route path="/famous-people" component={SelectedQuote}/>
                <Route path="/saying" component={SelectedQuote}/>
                <Route path="/humour" component={SelectedQuote}/>
                <Route path="/motivational" component={SelectedQuote}/>
                <Route render={() => <h1>404 Not Found</h1>}/>
            </Switch>
        </BrowserRouter>
    );
}

export default App;
