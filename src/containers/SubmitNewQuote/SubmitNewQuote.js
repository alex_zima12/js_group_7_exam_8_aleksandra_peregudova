import React, {useState} from 'react';
import axiosQuote from "../../axiosQuote";

const CATEGORIES = [
    {title: 'Star Wars', id: 'star-wars'},
    {title: 'Famous people', id: 'famous-people'},
    {title: 'Saying', id: 'saying'},
    {title: 'Humour', id: 'humour'},
    {title: 'Motivational', id: 'motivational'}
]

const SubmitNewQuote = props => {

    console.log(CATEGORIES[0].id);

    const [loading, setLoading] = useState(false);
    const [quotes, setQuotes] = useState({
        category: '',
        author: '',
        text: ''
    });

    const onChangeHandler = event => {
        const name = event.target.name;
        const value = event.target.value;
        setQuotes({...quotes, [name]: value});
    };

     const onChangeSelectHandler = event => {
         const value = event.target.value;
         setQuotes({...quotes, category: value});
     };

    const quoteHandler = async event => {
        event.preventDefault();
        setLoading(true);
        try {
            await axiosQuote.post('/quotes.json', quotes);
        } finally {
            setLoading(false);
            props.history.push('/');
        }
        ;
    };

    let form = (
        <form className="form-group my-5" onSubmit={quoteHandler}>
            <div className="form-group">
                <label htmlFor="exampleFormControlSelect1">Categories</label>
                <select
                    className="form-control"
                    id="exampleFormControlSelect1"
                    onChange={onChangeSelectHandler}
                >
                    <option value={CATEGORIES[0].id}>{CATEGORIES[0].title}</option>
                    <option value={CATEGORIES[1].id}>{CATEGORIES[1].title}</option>
                    <option value={CATEGORIES[2].id}>{CATEGORIES[2].title}</option>
                    <option value={CATEGORIES[3].id}>{CATEGORIES[3].title}</option>
                    <option value={CATEGORIES[4].id}>{CATEGORIES[4].title}</option>
                </select>
            </div>
            <div className="form-group">
                <label>Author</label>
                <input
                    type="text"
                    name="author"
                    className="form-control"
                    onChange={onChangeHandler}
                    required
                />
            </div>
            <div className="form-group">
                <label htmlFor="exampleFormControlTextarea1">Quote text</label>
                <textarea
                    className="form-control"
                    name="text"
                    id="exampleFormControlTextarea1"
                    rows="3"
                    onChange={onChangeHandler}
                    required
                />
            </div>
            <button type="submit" className="btn btn-primary">Save</button>
        </form>
    );

    if (loading) {
        return (
            <div className="text-center mt-5">
                <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        );
    }
    ;

    return quotes && (
        <div className="container">
            {form}
        </div>
    );
};

export default SubmitNewQuote;