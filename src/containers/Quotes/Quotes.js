import React, {useEffect, useState} from 'react';
import {NavLink} from "react-router-dom";
import axiosQuote from "../../axiosQuote";

const Quotes = () => {
    const [quotes, setQuotes] = useState([]);
    useEffect(() => {
        const getQuotesList = async () => {
            const quoteResponse = await axiosQuote.get('/quotes.json');
            setQuotes(quoteResponse.data);
        };
        getQuotesList().catch(console.error);
    }, []);

    const dataQuote = Object.keys(quotes).map((key) => {
        return (
            <div className="card my-5"
                 key={key}>
                <div className="card-header">
                    Quote
                </div>
                <div className="card-body">
                        <span>This quote is from the category
                            <b> {quotes[key].category} </b>
                        </span>
                    <h1>{quotes[key].text}</h1>
                    <p className="card-title">{quotes[key].author}</p>
                    <NavLink to={"/quote/" + key + "/edit"} className="btn btn-outline-info">Change</NavLink>
                </div>
            </div>
        );
    });

    return (
        <div className="container">
            <ul className="list-group mt-5">
                <li className="list-group-item active"><NavLink className="nav-link" to="/">All</NavLink></li>
                <li className="list-group-item"><NavLink className="nav-link" to="/star-wars">Star Wars</NavLink></li>
                <li className="list-group-item"><NavLink className="nav-link" to="/famous-people">Famous
                    people</NavLink></li>
                <li className="list-group-item"><NavLink className="nav-link" to="/saying">Saying</NavLink></li>
                <li className="list-group-item"><NavLink className="nav-link" to="/humour">Humour</NavLink></li>
                <li className="list-group-item"><NavLink className="nav-link" to="/motivational">Motivational</NavLink>
                </li>
            </ul>
            {dataQuote}
        </div>
    );
};

export default Quotes;