import React, {useEffect, useState} from 'react'
import axiosQuote from "../../axiosQuote";

const CATEGORIES = [
    {title: 'Star Wars', id: 'star-wars'},
    {title: 'Famous people', id: 'famous-people'},
    {title: 'Saying', id: 'saying'},
    {title: 'Humour', id: 'humour'},
    {title: 'Motivational', id: 'motivational'}
]

const EditQuote = props => {
    const [newAuthor, SetNewAuthor] = useState('');
    const [newText, SetNewText] = useState('');
    const [newCategory, SetNewCategory] = useState('');

    useEffect(() => {
        const getQuotesList = async () => {
            const quoteResponse = await axiosQuote.get('/quotes/' + props.match.params.id + '.json');
            SetNewAuthor(quoteResponse.data.author);
            SetNewText(quoteResponse.data.text);
            SetNewCategory(quoteResponse.data.category);
        };
        getQuotesList().catch(console.error);
    }, [props.match.params.id]);

    const changeAuthor = event => {
        SetNewAuthor(event.target.value);
    };

    const changeText = event => {
        SetNewText(event.target.value);
    };

    const changeCategory = event => {
        SetNewCategory(event.target.value);
    };

    const changeQuote = async (e, id) => {
        e.preventDefault();
        await axiosQuote.put('/quotes/' + id + '.json', {
            category: newCategory,
            author: newAuthor,
            text: newText
        });
        props.history.push('/');
    };

    const deleteQuote = async (e, id) => {
        e.preventDefault();
        await axiosQuote.delete('/quotes/' + id + '.json');
        props.history.push('/');
    };

    return (
        <form className="form-group my-5 container">
            <div className="form-group">
                <label htmlFor="exampleFormControlSelect1">Categories</label>
                <select
                    className="form-control"
                    id="exampleFormControlSelect1"
                    onChange={changeCategory}
                >
                    <option value={newCategory}>{newCategory}</option>
                    <option value={CATEGORIES[0].id}>{CATEGORIES[0].title}</option>
                    <option value={CATEGORIES[1].id}>{CATEGORIES[1].title}</option>
                    <option value={CATEGORIES[2].id}>{CATEGORIES[2].title}</option>
                    <option value={CATEGORIES[3].id}>{CATEGORIES[3].title}</option>
                    <option value={CATEGORIES[4].id}>{CATEGORIES[4].title}</option>
                </select>
            </div>
            <div className="form-group">
                <label>Author</label>
                <input
                    value={newAuthor}
                    type="text"
                    name="author"
                    className="form-control"
                    onChange={changeAuthor}
                    required
                />
            </div>
            <div className="form-group">
                <label htmlFor="exampleFormControlTextarea1">Quote text</label>
                <textarea
                    value={newText}
                    className="form-control"
                    name="text"
                    id="exampleFormControlTextarea1"
                    rows="3"
                    onChange={changeText}
                    required
                />
            </div>
            <button
                type="submit"
                className="btn btn-primary"
                onClick={(event) => changeQuote(event, props.match.params.id)}
            >Save
            </button>
            <button
                type="button"
                onClick={(e) => deleteQuote(e, props.match.params.id)}
                className="btn btn-danger ml-3"
            >Delete
            </button>
        </form>
    );
};

export default EditQuote;