import React, {useEffect, useState} from 'react';
import axiosQuote from "../../axiosQuote";
import {NavLink} from "react-router-dom";

const SelectedQuote = props => {
    const [quotes, setQuotes] = useState([]);
    useEffect(() => {
        const getQuotesList = async () => {
            let category = props.match.path.replace('/', '');
            const quoteResponse = await axiosQuote.get('/quotes.json?orderBy="category"&equalTo="' + category + '"');
            setQuotes(quoteResponse.data);
        };
        getQuotesList().catch(console.error);
    }, [props.match.path]);

    const dataQuote = Object.keys(quotes).map((key) => {
        return quotes && (
            <div className="card my-5"
                 key={key}>
                <div className="card-header">
                    Quote
                </div>
                <div className="card-body">
                    <span>This quote is from the category
                        <b> {quotes[key].category}</b>
                    </span>
                    <h1>{quotes[key].text}</h1>
                    <p className="card-title">{quotes[key].author}</p>
                    <NavLink
                        to={"/quote/" + key + "/edit"}
                        class="btn btn-outline-info">
                        Change
                    </NavLink>
                </div>
            </div>
        );
    });

    return (
        <div className="container">
            {dataQuote}
        </div>
    );
};

export default SelectedQuote;